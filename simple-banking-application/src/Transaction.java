public interface Transaction {

    double checkBalance();
    void deposit(double amount);
    void withdraw(double amount);
    void viewPreviousTransaction();
    void calculateInterest(int years);
}
