import java.text.DecimalFormat;
import java.util.Scanner;

public class Menu {

    void showMenu() {
        char option;
        Scanner scanner = new Scanner(System.in);
        Account account = new Account();
        DecimalFormat deciForm = new DecimalFormat("#.##");

        System.out.println("\nWhat would you like to do today?");

        do {
            System.out.println("\nOptions: ");
            System.out.println("A: Balance inquiry" +
                    "\nB: Deposit" +
                    "\nC: Withdrawal" +
                    "\nD: View previous transaction" +
                    "\nE: Calculate interest amount" +
                    "\nF: Exit\n");
            char option1 = scanner.next().charAt(0);
            option = Character.toUpperCase(option1);

            switch (option) {
                case 'A': //Balance inquiry
                    System.out.println("\n===========================================================================");
                    System.out.println("Balance = Php " + deciForm.format((account.checkBalance())));
                    System.out.println("===========================================================================");
                    break;

                case 'B': //Deposit
                    System.out.println("\n===========================================================================");
                    System.out.println("Please enter the amount that you want to deposit.");
                    double amount = scanner.nextDouble();
                    account.deposit(amount);
                    System.out.println("Desired amount: Php " + deciForm.format(amount));
                    System.out.println("===========================================================================");
                    break;

                case 'C': //Withdrawal
                    System.out.println("\n===========================================================================");
                    System.out.println("Please enter the amount that you want to withdraw.");
                    int amount2 = scanner.nextInt();
                    System.out.println("Desired amount: Php " + deciForm.format(amount2));
                    account.withdraw(amount2);
                    System.out.println("===========================================================================");
                    break;

                case 'D': //View previous transaction
                    System.out.println("\n===========================================================================");
                    account.viewPreviousTransaction();
                    System.out.println("===========================================================================");
                    break;

                case 'E': //Calculate interest amount
                    System.out.println("\n===========================================================================");
                    System.out.println("Please enter how many years of accrued interest.");
                    int years = scanner.nextInt();
                    account.calculateInterest(years);
                    System.out.println("===========================================================================");
                    break;

                case 'F': //Exit
                    System.out.println("\n===========================================================================");
                    break;

                default:
                    System.out.println("Invalid input.");
                    throw new IllegalStateException("Unexpected value: " + option);
            }
        }
        while (option != 'F'); {
            System.out.println("Thank you for banking with us!");
            System.out.println("===========================================================================");
        }
    }
}
