import java.text.DecimalFormat;

public class Account implements Transaction {

    private double currentBalance = 0;
    private double previousTransaction = 0;
    private String depositorName;
    private String depositorId;
    DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public String getDepositorName() {
        return depositorName;
    }
    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }
    public String getDepositorId() {
        return depositorId;
    }
    public void setDepositorId(String depositorId) {
        this.depositorId = depositorId;
    }

    @Override
    public double checkBalance() {
        return currentBalance;
    }

    @Override
    public void deposit(double amount) {
        if (amount != 0) {
            currentBalance = currentBalance + amount;
            previousTransaction = currentBalance;
        }
    }

    @Override
    public void withdraw(double amount) {
        if (amount > currentBalance) {
            System.out.println("Transaction cannot be processed." +
                    "\nYou cannot withdraw an amount higher than your current balance.");
        }
        else if (amount != 0) {
            currentBalance = currentBalance - amount;
            previousTransaction = -amount;
        }
    }

    @Override
    public void viewPreviousTransaction() {
        if (previousTransaction > 0) {
            System.out.println("You have deposited: Php " +
                    decimalFormat.format(previousTransaction));
        }
        else if (previousTransaction < 0) {
            System.out.println("You have withdrawn: Php " +
                    decimalFormat.format(Math.abs(previousTransaction)));
        }
        else {
            System.out.println("No previous transaction can be shown.");
        }
    }

    @Override
    public void calculateInterest(int years) {
        double interestRate = 0.0388D; //Current interest rate is 3.88% PA - CIMB Bank PH)
        double newBalance = currentBalance + (currentBalance * interestRate * years);
        System.out.println("Current interest rate per annum is: " + (100 * interestRate) + "%");
        System.out.println("After " + years + " year(s), your balance will yield to Php " +
                decimalFormat.format(newBalance) + " .");
    }
}
