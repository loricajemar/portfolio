public class PremierBank {

    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setDepositorId("PB210001");
        myAccount.setDepositorName("Jim");
        System.out.println("Hello " + myAccount.getDepositorName() + "!");
        System.out.println("Your ID is: " + myAccount.getDepositorId());

        Menu menu = new Menu();
        menu.showMenu();
        
    }
}
