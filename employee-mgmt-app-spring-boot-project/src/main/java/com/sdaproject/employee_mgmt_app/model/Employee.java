package com.sdaproject.employee_mgmt_app.model;
import java.io.Serializable;

public class Employee implements Serializable {
    private static final long serialVersionUID = -8809089768201955649L;
    private Long id;
    private String firstName;
    private String lastName;
    private Long phoneNumber;
    private String email;
    public Employee(){}

    public Employee(Long id, String firstName, String lastName, Long phoneNumber, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
