package com.sdaproject.employee_mgmt_app.controller;

import com.sdaproject.employee_mgmt_app.model.Employee;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping(value = "/api/employees")
public class EmployeeController {

    private List<Employee> employees = new ArrayList<Employee>();

    EmployeeController() {
        this.employees = buildEmployees();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> getEmployees() {
        return this.employees;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Employee getEmployee(@PathVariable("id") Long id) {
        return this.employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst()
                .orElse(null);
    }

    @RequestMapping(method = RequestMethod.POST)
    public List<Employee> addEmployee(@RequestBody Employee employee) {
        Long nextId = 0L;
        if (this.employees.size() != 0) {
            Employee lastEmployee = this.employees
                    .stream()
                    .skip(this.employees.size() - 1).findFirst()
                    .orElse(null);
            nextId = lastEmployee.getId() + 1;
        }
        employee.setId(nextId);
        this.employees.add(employee);
        return this.employees;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public List<Employee> deleteEmployee(@PathVariable Long id) {
        for(Iterator<Employee> iterator = this.employees.iterator(); iterator.hasNext();)
        {
            Employee employee = iterator.next();
            Long inId = employee.getId();
            if(inId == (id)){
                iterator.remove();
            }
        }
        return this.employees;
    }

    List<Employee> buildEmployees() {
        List<Employee> employees = new ArrayList<>();
        Employee employee1 = buildEmployee(1L,
                "Jim",
                "Lorica",
                9452773068L,
                "loricajemar@gmail.com");
        Employee employee2 = buildEmployee(2L,
                "Rhose",
                "Azcelle",
                9201234567L,
                "rhose.azcelle@example.com");
        employees.add(employee1);
        employees.add(employee2);
        return employees;
    }

    Employee buildEmployee(Long id, String firstName,String lastName, Long phoneNumber, String email) {
        Employee employee = new Employee(id, firstName, lastName, phoneNumber, email);
        return employee;
    }

}
