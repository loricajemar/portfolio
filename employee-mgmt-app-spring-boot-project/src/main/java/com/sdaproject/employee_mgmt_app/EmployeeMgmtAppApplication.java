package com.sdaproject.employee_mgmt_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeMgmtAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeMgmtAppApplication.class, args);
    }

}
