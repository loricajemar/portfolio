export class Employee {
  id: number;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;

  constructor(id: number, firstName: string, lastName: string, phoneNumber: string, email: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }
}

