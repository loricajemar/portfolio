import { Injectable } from '@angular/core';
import { Employee} from './employee';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {
  private apiUrl = '/api/employees';
  constructor(private http: Http) {
  }

  findAll(): Promise<Array<Employee>> {
    return this.http.get(this.apiUrl)
      .toPromise()
      .then(response => response.json() as Employee[])
      .catch(this.handleError);
  }

  addEmployee(employee: Employee): Promise<Array<Employee>> {
    let employeeHeaders = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.apiUrl, JSON.stringify(employee), { headers: employeeHeaders })
      .toPromise()
      .then(response => response.json() as Employee[])
      .catch(this.handleError);
  }

  deleteEmployeeById(id: number): Promise<Array<Employee>> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete(url)
      .toPromise()
      .then(response => response.json() as Employee[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<Array<any>> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
